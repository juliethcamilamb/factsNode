const express = require("express");
const request = require("request");
const app = express();

app.get("/service", (req, res) => {
    request("https://catfact.ninja/facts", (err, resp, body) => {
        if (!err) {
            const list = JSON.parse(body);
            var response = [];
            response.push(list);
            res.send(response);
        } else {
            console.log("Error service facts");
        }
    })
});

app.listen(3000, () => {
    console.log("App services port 3000");
});